%define debug_package   %{nil}
%global _hardened_build 1

%global embeddedTar     %{name}.tar.bz2

%global forgejoDocDir     %{_docdir}/%{name}
%global forgejoShareDir   %{_datarootdir}/%{name}
%global forgejoStateDir   %{_sharedstatedir}/%{name}
%global forgejoUser       git

Name:           forgejo
Version:        10.0.1
Release:        1.3.gee49a62bed%{?dist}

License:        MIT
URL:            https://gitlab.com/fhuberts/rpmsUpstream
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  crudini
BuildRequires:  git, gzip
BuildRequires:  golang
BuildRequires:  make
BuildRequires:  npm
BuildRequires:  nodejs
BuildRequires:  pam-devel
BuildRequires:  systemd
BuildRequires:  systemd-rpm-macros




#%package
Summary:        Forgejo (Beyond coding. We forge.)

Requires:       asciidoc
Requires:       checkpolicy, crudini
Requires:       coreutils
Requires:       gawk, git, grep
Requires:       firewalld
Requires:       hostname, httpd
Requires:       make, mod_ssl
Requires:       openssh-server
Requires:       policycoreutils
Requires:       postgresql, postgresql-server, postgresql-contrib
Requires:       postgresql-upgrade
Requires:       pwgen
Requires:       sed, shadow-utils, sudo, systemd

Conflicts:      gitea


%package labelset-advanced
Summary:        Advanced labelset for Forgejo

BuildArch:      noarch

Requires:       %{name}

Conflicts:      gitea-labelset-advanced


%package labelset-trac
Summary:        Trac labelset for Forgejo

BuildArch:      noarch

Requires:       %{name}

Conflicts:      gitea-labelset-trac


%package redirect-home
Summary:        Redirect the webserver home page to Forgejo

BuildArch:      noarch

Requires:       %{name}

Conflicts:      gitea-redirect-home


%package webhooks
Summary:        A collection of extra Forgejo webhooks

BuildArch:      noarch

Requires:       %{name}
Requires:       httpd
Requires:       php

Conflicts:      gitea-webhooks


%description
Forgejo (Beyond coding. We forge.)

Forgejo is a self-hosted lightweight software forge.
Easy to install and low maintenance, it just does the job.

Brought to you by an inclusive community under the umbrella of Codeberg e.V.,
a democratic non-profit organization, Forgejo can be trusted to be exclusively
Free Software. It focuses on security, scaling, federation and privacy. Learn
more about how it compares with other forges.

Upstream     : https://forgejo.org/
Upstream code: https://codeberg.org/forgejo/forgejo.git


%description labelset-advanced
An advanced labelset that allows mapping of status, kind, reviewed and
priority.


%description labelset-trac
A labelset that allows mapping of Trac status, ticket type, resolution and
priority.


%description redirect-home
Redirect the webserver home page to Forgejo


%description webhooks
A collection of extra Forgejo webhooks.

Do read the documentation in %{forgejoDocDir}/webhooks to learn how to
use the webhooks.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}
mkdir build
cd build
tar jxvf "../%{embeddedTar}"
cp -v ../VERSION .


%build
cd build

  _LDFLAGS="-X \"code.gitea.io/gitea/modules/setting.AppWorkPath=%{forgejoStateDir}\""
_LDFLAGS+=" -X \"code.gitea.io/gitea/modules/setting.CustomPath=%{forgejoStateDir}/custom\""
_LDFLAGS+=" -X \"code.gitea.io/gitea/modules/setting.PIDFile=/run/%{name}/%{name}.pid\""
_LDFLAGS+=" -X \"code.gitea.io/gitea/modules/setting.CustomConf=%{_sysconfdir}/%{name}/app.ini\""

_version="$(head -1 VERSION 2> /dev/null)"

#        Version     = "development" // program version for this build
sed -ri \
  "s/^([[:space:]]*Version[[:space:]]*=[[:space:]]*\")[^\"]*(\"[[:space:]]*)(\/\/.*)?\$/\1$_version\2\3/" \
  main.go \
  2> /dev/null

export GOPROXY=http://proxy.golang.org
make \
  TAGS="bindata sqlite sqlite_unlock_notify pam" \
  LDFLAGS="$_LDFLAGS" \
  vendor build


%install
cd build
rm -rfv "%{buildroot}"
mkdir -pv \
  "%{buildroot}" \
  "%{buildroot}%{_sbindir}" \
  "%{buildroot}%{forgejoDocDir}" \
  "%{buildroot}%{forgejoShareDir}" \
  "%{buildroot}%{forgejoStateDir}" \
  "%{buildroot}%{forgejoStateDir}/custom" \
  "%{buildroot}%{forgejoStateDir}/data" \
  "%{buildroot}%{forgejoStateDir}/data/home" \
  "%{buildroot}%{forgejoStateDir}/log"


cp -pdrv -t "%{buildroot}"                    "../etc" "../usr" "../var"
mv "gitea" "%{name}"
cp -pdrv -t "%{buildroot}%{_sbindir}"         "%{name}"
cp -pdrv -t "%{buildroot}%{forgejoDocDir}" \
  CODEOWNERS DCO LICENSE *.md
cp -pdrv -t "%{buildroot}%{forgejoShareDir}" \
  "contrib/legal" \
  "contrib/options" \
  "custom/conf" \
  "templates"
cp -pdrv -t "%{buildroot}%{forgejoStateDir}/custom/options/label" \
  "contrib/options/label/Advanced"

mkdir -p "%{buildroot}%{_mandir}/man8"
"%{buildroot}%{_sbindir}/%{name}" \
  docs \
  --man \
  --output "%{buildroot}%{_mandir}/man8/%{name}.8"
gzip "%{buildroot}%{_mandir}/man8/%{name}.8"

iniFile="%{buildroot}%{_sysconfdir}/%{name}/app.ini"
mkdir -p "$(dirname "$iniFile")"
cp "custom/conf/app.example.ini" "$iniFile"

crudini --set --inplace "$iniFile" "server" "PROTOCOL" "http+unix"
crudini --set --inplace "$iniFile" "server" "DOMAIN" "placeholder"
crudini --set --inplace "$iniFile" "server" "ROOT_URL" "https://%%(DOMAIN)s/git/"
crudini --set --inplace "$iniFile" "server" "HTTP_ADDR" "/run/%{name}/%{name}.sock"
crudini --set --inplace "$iniFile" "server" "HTTP_PORT" ""
crudini --del --inplace "$iniFile" "server" "LOCAL_ROOT_URL" #"%%(PROTOCOL)s://%%(HTTP_ADDR)s:%%(HTTP_PORT)s/"
crudini --set --inplace "$iniFile" "server" "SSH_LISTEN_HOST" "0.0.0.0"
crudini --set --inplace "$iniFile" "server" "SSH_ROOT_PATH" "placeholder"
crudini --set --inplace "$iniFile" "server" "SSH_AUTHORIZED_KEYS_BACKUP" "true"
crudini --set --inplace "$iniFile" "server" "OFFLINE_MODE" "true"
crudini --set --inplace "$iniFile" "server" "ENABLE_GZIP" "true"
crudini --set --inplace "$iniFile" "server" "LFS_START_SERVER" "true"
crudini --set --inplace "$iniFile" "server" "LFS_JWT_SECRET" "placeholder"
crudini --set --inplace "$iniFile" "database" "DB_TYPE" "postgres"
crudini --set --inplace "$iniFile" "database" "HOST" "127.0.0.1:5432"
crudini --set --inplace "$iniFile" "database" "NAME" "%{name}"
crudini --set --inplace "$iniFile" "database" "USER" "%{name}"
crudini --set --inplace "$iniFile" "database" "PASSWD" "placeholder"
crudini --set --inplace "$iniFile" "security" "INSTALL_LOCK" "true"
crudini --set --inplace "$iniFile" "security" "SECRET_KEY" "placeholder"
crudini --set --inplace "$iniFile" "security" "INTERNAL_TOKEN" "placeholder"
crudini --del --inplace "$iniFile" "oauth2" "ENABLE"
crudini --set --inplace "$iniFile" "oauth2" "JWT_SECRET" "placeholder"
crudini --set --inplace "$iniFile" "log" "LEVEL" "Warn"
crudini --set --inplace "$iniFile" "service" "REGISTER_EMAIL_CONFIRM" "true"
crudini --set --inplace "$iniFile" "service" "REQUIRE_SIGNIN_VIEW" "true"
crudini --set --inplace "$iniFile" "service" "ENABLE_NOTIFY_MAIL" "true"
crudini --set --inplace "$iniFile" "service" "DEFAULT_ALLOW_CREATE_ORGANIZATION" "false"
crudini --set --inplace "$iniFile" "service" "DEFAULT_USER_IS_RESTRICTED" "true"
crudini --set --inplace "$iniFile" "service" "DEFAULT_USER_VISIBILITY" "limited"
crudini --set --inplace "$iniFile" "service" "DEFAULT_ORG_VISIBILITY" "limited"
crudini --set --inplace "$iniFile" "repository" "ROOT" "%{forgejoStateDir}/data/repositories"
crudini --set --inplace "$iniFile" "repository" "PREFERRED_LICENSES" "GPL-2.0-only,GPL-2.0-or-later,GPL-3.0-linking-exception,GPL-3.0-linking-source-exception,GPL-3.0-only,GPL-3.0-or-later,LGPL-2.0-only,LGPL-2.0-or-later,LGPL-2.1-only,LGPL-2.1-or-later,LGPL-3.0-linking-exception,LGPL-3.0-only,LGPL-3.0-or-later,GPL-CC-1.0,Apache-2.0,MIT"
crudini --set --inplace "$iniFile" "repository.upload" "ALLOWED_TYPES" "*/*"
crudini --set --inplace "$iniFile" "repository.upload" "FILE_MAX_SIZE" "1000"
crudini --set --inplace "$iniFile" "repository.upload" "MAX_FILES" "25"
crudini --set --inplace "$iniFile" "repository.pull-request" "DEFAULT_MERGE_MESSAGE_COMMITS_LIMIT" "-1"
crudini --set --inplace "$iniFile" "repository.pull-request" "DEFAULT_MERGE_MESSAGE_SIZE" "-1"
crudini --set --inplace "$iniFile" "repository.pull-request" "DEFAULT_MERGE_MESSAGE_ALL_AUTHORS" "true"
crudini --set --inplace "$iniFile" "repository.pull-request" "DEFAULT_MERGE_MESSAGE_MAX_APPROVERS" "-1"
crudini --set --inplace "$iniFile" "repository.pull-request" "ADD_CO_COMMITTER_TRAILERS" "false"
crudini --set --inplace "$iniFile" "repository.release" "ALLOWED_TYPES" "*/*"
crudini --set --inplace "$iniFile" "project" "PROJECT_BOARD_BASIC_KANBAN_TYPE" "To Do, In Progress, Review, Done"
crudini --set --inplace "$iniFile" "project" "PROJECT_BOARD_BUG_TRIAGE_TYPE" "Needs Triage, High Priority, Medium Priority, Low Priority, Closed"
crudini --set --inplace "$iniFile" "ui" "EXPLORE_PAGING_NUM" "50"
crudini --set --inplace "$iniFile" "ui" "ISSUE_PAGING_NUM" "50"
crudini --set --inplace "$iniFile" "ui" "FEED_MAX_COMMIT_NUM" "10"
crudini --set --inplace "$iniFile" "ui" "FEED_PAGING_NUM" "50"
crudini --set --inplace "$iniFile" "ui" "SITEMAP_PAGING_NUM" "50"
crudini --set --inplace "$iniFile" "ui" "GRAPH_MAX_COMMIT_NUM" "500"
crudini --set --inplace "$iniFile" "ui" "DEFAULT_SHOW_FULL_NAME" "true"
crudini --set --inplace "$iniFile" "ui.admin" "NOTICE_PAGING_NUM" "50"
crudini --set --inplace "$iniFile" "ui.user" "REPO_PAGING_NUM" "50"
crudini --set --inplace "$iniFile" "indexer" "REPO_INDEXER_ENABLED" "true"
crudini --set --inplace "$iniFile" "indexer" "REPO_INDEXER_REPO_TYPES" "sources,forks,mirrors,templates"
crudini --del --inplace "$iniFile" "queue" "CONN_STR"
crudini --set --inplace "$iniFile" "openid" "ENABLE_OPENID_SIGNIN" "false"
crudini --set --inplace "$iniFile" "openid" "ENABLE_OPENID_SIGNUP" "false"
crudini --set --inplace "$iniFile" "webhook" "ALLOWED_HOST_LIST" "*"
crudini --set --inplace "$iniFile" "mailer" "ENABLED" "true"
crudini --set --inplace "$iniFile" "mailer" "PROTOCOL" "sendmail"
crudini --set --inplace "$iniFile" "mailer" "FROM" "placeholder"
crudini --set --inplace "$iniFile" "mailer" "ENVELOPE_FROM" "placeholder"
crudini --set --inplace "$iniFile" "mailer" "SENDMAIL_PATH" "/usr/sbin/sendmail"
crudini --set --inplace "$iniFile" "session" "PROVIDER" "file"
crudini --set --inplace "$iniFile" "picture" "DISABLE_GRAVATAR" "true"
crudini --set --inplace "$iniFile" "attachment" "ALLOWED_TYPES" "*/*"
crudini --set --inplace "$iniFile" "attachment" "MAX_SIZE" "2048"
crudini --set --inplace "$iniFile" "attachment" "MAX_FILES" "25"
crudini --set --inplace "$iniFile" "cron.archive_cleanup" "RUN_AT_START" "false"
crudini --set --inplace "$iniFile" "cron.update_mirrors" "PULL_LIMIT" "1000"
crudini --set --inplace "$iniFile" "cron.update_mirrors" "PUSH_LIMIT" "1000"
crudini --set --inplace "$iniFile" "cron.repo_health_check" "TIMEOUT" "600s"
crudini --set --inplace "$iniFile" "cron.check_repo_stats" "RUN_AT_START" "false"
crudini --set --inplace "$iniFile" "cron.update_migration_poster_id" "RUN_AT_START" "false"
crudini --set --inplace "$iniFile" "cron.cleanup_actions" "RUN_AT_START" "false"
crudini --set --inplace "$iniFile" "cron.deleted_branches_cleanup" "RUN_AT_START" "false"
crudini --set --inplace "$iniFile" "cron.cleanup_packages" "RUN_AT_START" "false"
crudini --set --inplace "$iniFile" "cron.git_gc_repos" "ENABLED" "true"
crudini --set --inplace "$iniFile" "cron.git_gc_repos" "TIMEOUT" "600s"
crudini --set --inplace "$iniFile" "cron.update_checker" "ENABLED" "false"
crudini --set --inplace "$iniFile" "cron.gc_lfs" "ENABLED" "true"
crudini --set --inplace "$iniFile" "git.timeout" "DEFAULT" "600"
crudini --set --inplace "$iniFile" "git.timeout" "MIGRATE" "3600"
crudini --set --inplace "$iniFile" "git.timeout" "MIRROR" "3600"
crudini --set --inplace "$iniFile" "git.timeout" "CLONE" "600"
crudini --set --inplace "$iniFile" "git.timeout" "PULL" "600"
crudini --set --inplace "$iniFile" "git.timeout" "GC" "1800"
crudini --set --inplace "$iniFile" "git.timeout" "GREP" "60"
crudini --set --inplace "$iniFile" "git.config" "core.commitGraph" "true"
crudini --set --inplace "$iniFile" "git.config" "core.logAllRefUpdates" "true"
crudini --set --inplace "$iniFile" "git.config" "gc.reflogExpire" "90"
crudini --set --inplace "$iniFile" "git.config" "gc.writeCommitGraph" "true"
crudini --set --inplace "$iniFile" "mirror" "DEFAULT_INTERVAL" "1h"
crudini --set --inplace "$iniFile" "markup" "MERMAID_MAX_SOURCE_CHARACTERS" "-1"
crudini --set --inplace "$iniFile" "metrics" "ENABLED_ISSUE_BY_LABEL" "true"
crudini --set --inplace "$iniFile" "metrics" "ENABLED_ISSUE_BY_REPOSITORY" "true"
crudini --set --inplace "$iniFile" "migrations" "ALLOW_LOCALNETWORKS" "true"


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%pre

if [[ $1 -eq 1 ]]; then
  # install

  id "%{forgejoUser}" &>/dev/null
  if [[ $? -ne 0 ]]; then
    useradd \
      --system \
      --shell "/usr/bin/bash" \
      --comment "Git Version Control" \
      --user-group \
      --home-dir "%{forgejoStateDir}" \
      "%{forgejoUser}"
  fi
fi

exit 0


%post

function readAppIni() {
  local iniFile="$1"

  PKG_DOMAIN="$(        crudini --get "$iniFile" "server"   "DOMAIN"         2> /dev/null)"
  PKG_SSH_ROOT_PATH="$( crudini --get "$iniFile" "server"   "SSH_ROOT_PATH"  2> /dev/null)"
  PKG_LFS_JWT_SECRET="$(crudini --get "$iniFile" "server"   "LFS_JWT_SECRET" 2> /dev/null)"
  PKG_PASSWD="$(        crudini --get "$iniFile" "database" "PASSWD"         2> /dev/null)"
  PKG_SECRET_KEY="$(    crudini --get "$iniFile" "security" "SECRET_KEY"     2> /dev/null)"
  PKG_INTERNAL_TOKEN="$(crudini --get "$iniFile" "security" "INTERNAL_TOKEN" 2> /dev/null)"
  PKG_FROM="$(          crudini --get "$iniFile" "mailer"   "FROM"           2> /dev/null)"
  PKG_ENVELOPE_FROM="$( crudini --get "$iniFile" "mailer"   "ENVELOPE_FROM"  2> /dev/null)"
  PKG_JWT_SECRET="$(    crudini --get "$iniFile" "oauth2"   "JWT_SECRET"     2> /dev/null)"
}

function updateAppIni() {
  local iniFile="$1"

  crudini --set --inplace "$iniFile" "server"   "DOMAIN"         "$PKG_DOMAIN"         2> /dev/null
  crudini --set --inplace "$iniFile" "server"   "SSH_ROOT_PATH"  "$PKG_SSH_ROOT_PATH"  2> /dev/null
  crudini --set --inplace "$iniFile" "server"   "LFS_JWT_SECRET" "$PKG_LFS_JWT_SECRET" 2> /dev/null
  crudini --set --inplace "$iniFile" "database" "PASSWD"         "$PKG_PASSWD"         2> /dev/null
  crudini --set --inplace "$iniFile" "security" "SECRET_KEY"     "$PKG_SECRET_KEY"     2> /dev/null
  crudini --set --inplace "$iniFile" "security" "INTERNAL_TOKEN" "$PKG_INTERNAL_TOKEN" 2> /dev/null
  crudini --set --inplace "$iniFile" "mailer"   "FROM"           "$PKG_FROM"           2> /dev/null
  crudini --set --inplace "$iniFile" "mailer"   "ENVELOPE_FROM"  "$PKG_ENVELOPE_FROM"  2> /dev/null
  crudini --set --inplace "$iniFile" "oauth2"   "JWT_SECRET"     "$PKG_JWT_SECRET"     2> /dev/null
}

function updateHostFqdn() {
  local confFile="$1"
  local hostnameFqdn="$(hostname -f 2>/dev/null)"

  sed -i "s/__host_fqdn__/$hostnameFqdn/g" "$confFile"
}

if [[ $1 -eq 2 ]]; then
  # upgrade

  iniFile="%{_sysconfdir}/%{name}/app.ini"
  iniFileNew="$iniFile.rpmnew"
  if [[ -f "$iniFile" ]] && [[ -f "$iniFileNew" ]]; then
    readAppIni "$iniFile"

    # Upgrade 1.15 to 1.16
    if [[ -z "$PKG_ENVELOPE_FROM" ]]; then
      PKG_ENVELOPE_FROM="%{forgejoUser}@$(hostname)"
    fi

    updateAppIni "$iniFileNew"
  fi

  # Upgrade actions indicated by 'sudo -u git forgejo doctor --all' (also check doctor.log!)
  if [[ "$(crudini --get "$iniFile" "server" "PROTOCOL" 2> /dev/null)" == "unix" ]]; then
    crudini --set --inplace "$iniFile" "server" "PROTOCOL" "http+unix"
  fi

  updateHostFqdn "/etc/httpd/conf.d/%{name}-redirect.conf"
  updateHostFqdn "/etc/httpd/conf.d/%{name}.conf"

  systemctl -q try-reload-or-restart \
    "%{name}.service" \
    "httpd.service" \
    "sshd.service"
fi

postgresHba="/var/lib/pgsql/data/pg_hba.conf"
postgresHbaLocalMethodRegex='^([[:space:]]*local[[:space:]]+all[[:space:]]+all[[:space:]]*)(.+)[[:space:]]*$'

if [[ $1 -eq 1 ]]; then
  # install

  # only setup postgresql when it is not yet defined
  postgresqlUnitExists="$(systemctl list-units --full -all 2> /dev/null | grep "postgresql.service" 2> /dev/null)"
  if [[ -z "$postgresqlUnitExists" ]]; then
    postgresql-setup --initdb --unit postgresql &> /dev/null

    systemctl -q enable \
      "postgresql.service"

    systemctl -q reload-or-restart \
      "postgresql.service"
  fi

  # Get the local method
  postgresHbaLocalMethodPrev="$(grep -E "$postgresHbaLocalMethodRegex" "$postgresHba" 2> /dev/null | sed -r "s/$postgresHbaLocalMethodRegex/\2/" 2> /dev/null)"

  # Set the local method to trust
  cp -a "$postgresHba" "$postgresHba.%{name}-install"
  sed -ri "s/$postgresHbaLocalMethodRegex/\1trust/" "$postgresHba"

  # Restart postgresql
  systemctl -q reload-or-restart "postgresql.service"

  # Check if we have to create the role and the database
  roleExists="$(sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='%{name}'" 2> /dev/null)"
  dbExists="$(  sudo -u postgres psql -lqt 2> /dev/null | awk '{print $1}' 2> /dev/null | grep '%{name}' 2> /dev/null)"

  newDbPassword="$(pwgen 16 1 2> /dev/null)"

  if [[ -z "$roleExists" ]]; then
    sudo -u postgres psql &> /dev/null << EOF
CREATE ROLE %{name} LOGIN ENCRYPTED PASSWORD '$newDbPassword';
EOF
  fi

  if [[ -z "$dbExists" ]]; then
    sudo -u postgres psql &> /dev/null << EOF
CREATE DATABASE %{name} OWNER %{name};
EOF
  fi

  if [[ -z "$roleExists" ]] || [[ -z "$dbExists" ]]; then
    sudo -u postgres psql &> /dev/null << EOF
GRANT ALL PRIVILEGES ON DATABASE %{name} TO %{name};
EOF
  fi

  forgejoLine="$(printf "%%-7s %%-15s %%-15s %%-23s %%s" \
               "host" \
               "%{name}" \
               "%{name}" \
               "127.0.0.1/32" \
               "md5" 2> /dev/null)"
  regex="^[[:space:]]*$(echo "$forgejoLine" | \
                        sed -r 's/[[:space:]]+/[[:space:]]+/g' 2> /dev/null)[[:space:]]*\$"
  currentLine="$(grep -niE "$regex" "$postgresHba" 2> /dev/null)"
  if [[ -z "$currentLine" ]]; then
    regexLocal="^[[:space:]]*local[[:space:]]+.*[[:space:]]*\$"
    currentLocalLine="$(grep -niE "$regexLocal" "$postgresHba" 2> /dev/null)"

    lineNr="1"
    sedMode="i"
    if [[ -n "$currentLocalLine" ]]; then
      lineNr="$(echo "$currentLocalLine" | \
                head -1 2> /dev/null | \
                awk -F ':' '{print $1}' 2> /dev/null)"
      sedMode="a"
    fi

    sed -i "$lineNr $sedMode $forgejoLine" "$postgresHba"
  fi

  # Restore the local method
  sed -ri "s/$postgresHbaLocalMethodRegex/\1$postgresHbaLocalMethodPrev/" "$postgresHba"
  rm -f "$postgresHba.%{name}-install"

  # Restart postgresql
  systemctl -q reload-or-restart "postgresql.service"

  sudo -u git git config -f "%{forgejoStateDir}/data/home/.gitconfig" user.name "Forgejo"
  sudo -u git git config -f "%{forgejoStateDir}/data/home/.gitconfig" user.email "%{forgejoUser}@$(hostname)"

  PKG_DOMAIN="$(hostname)"
  PKG_SSH_ROOT_PATH="%{forgejoStateDir}/.ssh"
  PKG_LFS_JWT_SECRET="$(%{name} generate secret LFS_JWT_SECRET 2> /dev/null | tail -1 2> /dev/null)"
  PKG_PASSWD="$newDbPassword"
  PKG_SECRET_KEY="$(%{name} generate secret SECRET_KEY 2> /dev/null | tail -1 2> /dev/null)"
  PKG_INTERNAL_TOKEN="$(%{name} generate secret INTERNAL_TOKEN 2> /dev/null | tail -1 2> /dev/null)"
  PKG_FROM="%{forgejoUser}@$(hostname)"
  PKG_ENVELOPE_FROM="%{forgejoUser}@$(hostname)"
  PKG_JWT_SECRET="$(%{name} generate secret JWT_SECRET 2> /dev/null | tail -1 2> /dev/null)"

  updateAppIni "%{_sysconfdir}/%{name}/app.ini"

  updateHostFqdn "/etc/httpd/conf.d/%{name}-redirect.conf"
  updateHostFqdn "/etc/httpd/conf.d/%{name}.conf"

  make -s -C "%{forgejoShareDir}/selinux" install

  for i in "" "--permanent"; do
    firewall-cmd -q $i \
      --add-service=http \
      --add-service=https
  done

  systemctl -q enable \
    "%{name}.service" \
    "httpd.service" \
    "sshd.service"

  systemctl -q reload-or-restart \
    "%{name}.service" \
    "httpd.service" \
    "sshd.service"
fi

exit 0


%post labelset-advanced

# install and upgrade

systemctl -q try-reload-or-restart \
  "%{name}.service"

exit 0


%post labelset-trac

# install and upgrade

systemctl -q try-reload-or-restart \
  "%{name}.service"

exit 0


%post redirect-home

function updateHostFqdn() {
  local confFile="$1"
  local hostnameFqdn="$(hostname -f 2>/dev/null)"

  sed -i "s/__host_fqdn__/$hostnameFqdn/g" "$confFile"
}

# install and upgrade

updateHostFqdn "/etc/httpd/conf.d/%{name}-redirect-home.conf"

sed -i 's/#__redirecthome__//g' "/etc/httpd/conf.d/%{name}.conf"

systemctl -q try-reload-or-restart \
  "httpd.service"

exit 0


%post webhooks

# install and upgrade

setsebool -P httpd_can_network_connect 1

systemctl -q try-reload-or-restart \
  "httpd.service"

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q stop    "%{name}.service"
  systemctl -q disable "%{name}.service"
fi

exit 0


%postun

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q try-reload-or-restart \
    "httpd.service" \
    "sshd.service"
fi

exit 0


%postun labelset-advanced

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q try-reload-or-restart \
    "%{name}.service"
fi

exit 0


%postun labelset-trac

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q try-reload-or-restart \
    "%{name}.service"
fi

exit 0


%postun redirect-home

if [[ $1 -eq 0 ]]; then
  # uninstall

  sed -ri \
    -e 's/^([[:space:]]+)(RewriteEngine)/\1#__redirecthome__\2/g' \
    -e 's/^([[:space:]]+)(RewriteRule)/\1#__redirecthome__\2/g' \
    "/etc/httpd/conf.d/%{name}.conf"

  systemctl -q try-reload-or-restart "httpd.service"
fi

exit 0


%postun webhooks

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q try-reload-or-restart "httpd.service"
fi

exit 0


%clean
rm -rf "%{buildroot}"


%files
%defattr(-,root,root,-)
%config(noreplace) %attr(660,root,%{forgejoUser}) %{_sysconfdir}/%{name}/app.ini
%{_sysconfdir}/httpd/conf.d/%{name}.conf
%{_sysconfdir}/httpd/conf.d/%{name}-redirect.conf
%{_sysconfdir}/ssh/sshd_config.d/%{name}.conf
%{_sbindir}/*
%{_unitdir}/*
%{forgejoDocDir}/CODEOWNERS
%{forgejoDocDir}/DCO
%{forgejoDocDir}/LICENSE
%{forgejoDocDir}/*.md
%{forgejoDocDir}/migrating/*
%{forgejoDocDir}/postgresql/*
%{forgejoDocDir}/upgrading/*
%{forgejoShareDir}/conf/*
%{forgejoShareDir}/legal/*
%{forgejoShareDir}/options/*
%{forgejoShareDir}/selinux/*
%{forgejoShareDir}/templates/*
%dir %attr(750,%{forgejoUser},%{forgejoUser}) %{forgejoStateDir}
%dir %attr(750,%{forgejoUser},%{forgejoUser}) %{forgejoStateDir}/custom
%dir %attr(750,%{forgejoUser},%{forgejoUser}) %{forgejoStateDir}/data
%dir %attr(750,%{forgejoUser},%{forgejoUser}) %{forgejoStateDir}/data/home
%dir %attr(750,%{forgejoUser},%{forgejoUser}) %{forgejoStateDir}/log
%{forgejoStateDir}/custom/robots.txt

%lang(en) %{_mandir}/man8/*.gz


%files labelset-advanced
%defattr(-,root,root,-)
%{forgejoStateDir}/custom/options/label/Advanced


%files labelset-trac
%defattr(-,root,root,-)
%{forgejoStateDir}/custom/options/label/Trac


%files redirect-home
%defattr(-,root,root,-)
%{_sysconfdir}/httpd/conf.d/%{name}-redirect-home.conf


%files webhooks
%defattr(-,root,root,-)
%{_sysconfdir}/httpd/conf.d/%{name}-webhooks.conf
%{forgejoDocDir}/webhooks/*
%{forgejoShareDir}/webhooks/*


%changelog
* Sun Feb 09 2025 Ferry Huberts - 10.0.1-1.3.gee49a62bed
- Update to v10.0.1-3-gee49a62bed

* Thu Jan 16 2025 Ferry Huberts - 10.0.0-1.1.g05056b8aa2
- Update to v10.0.0-1-g05056b8aa2

* Tue Dec 31 2024 Ferry Huberts - 9.0.3-3
- Reduce the number of redirects
- Improve redirect-home
- Make (podman) container registry work

* Sun Dec 15 2024 Ferry Huberts - 9.0.3-1
- Update to v9.0.3

* Wed Nov 20 2024 Ferry Huberts - 9.0.2-1
- Update to v9.0.2

* Fri Nov 01 2024 Ferry Huberts - 9.0.1-1
- Update to v9.0.1

* Wed Oct 16 2024 Ferry Huberts - 9.0.0-1
- Update to v9.0.0

* Thu Sep 19 2024 Ferry Huberts - 8.0.3-1
- Update to v8.0.3

* Thu Aug 29 2024 Ferry Huberts - 8.0.2-1
- Update to v8.0.2

* Fri Aug 09 2024 Ferry Huberts - 8.0.1-1
- Update to v8.0.1

* Wed Jul 31 2024 Ferry Huberts - 8.0.0-1.3.g8493433cfb
- Update to v8.0.0-3-g8493433cfb

* Wed Jul 31 2024 Ferry Huberts - 7.0.6-1
- Update to v7.0.6

* Thu Jul 04 2024 Ferry Huberts - 7.0.5-1
- Update to v7.0.5

* Sun Jun 16 2024 Ferry Huberts - 7.0.4-1
- Update to v7.0.4

* Fri May 24 2024 Ferry Huberts - 7.0.3-1
- Update to v7.0.3

* Fri May 10 2024 Ferry Huberts - 7.0.2-1
- Update to v7.0.2

* Sun May 05 2024 Ferry Huberts - 1.21.11.1-1.3.gf8be8e3b21
- Update to v1.21.11-1-3-gf8be8e3b21
