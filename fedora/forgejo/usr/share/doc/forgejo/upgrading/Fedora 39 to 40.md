# Upgrading from Fedora 39 to 40

Fedora 40 has a new version of the PostgreSQL database server.

Upgrading to that version requires a few manual steps, which are described
in this document.

* Before upgrading Fedora, disable the PostgreSQL service:

    ```
    [root@host ~]# systemctl disable --now postgresql.service
    ```

* Ensure the PostgreSQL upgrade tools are installed:

    ```
    [root@host ~]# dnf install -y postgresql-upgrade
    ```

* Upgrade Fedora

* Stop a few services:

    ```
    [root@host ~]# systemctl stop postgresql forgejo httpd
    ```

* Run the database upgrade:

    ```
    [root@host ~]# postgresql-setup --upgrade
    ```

* Check that the database configuration upgrade was performed correctly.

  For a plain installation the command below should show no differences with
  any real impact. However, if the database configuration was modified then
  those same modifications should probably be applied again, but now to the
  migrated configuration file (/var/lib/pgsql/data/postgresql.conf).

  Run the following command to show the differences between the original and
  migrated database configuration files:

    ```
    [root@host ~]# diff -urN /var/lib/pgsql/data{-old,}/postgresql.conf
    ```

* Restore the original database HBA configuration file:

    ```
    [root@host ~]# cp /var/lib/pgsql/data{-old,}/pg_hba.conf
    ```

* Enable the PostgreSQL service again:

    ```
    [root@host ~]# systemctl enable --now postgresql.service
    ```

* Fix the collation on all databases by running:

    ```
    [root@host ~]# /usr/share/doc/forgejo/postgresql/postgresql-collation
    ```

* Start the (previously stopped) services and check their statuses:

    ```
    [root@host ~]# systemctl restart forgejo httpd
    [root@host ~]# systemctl status  forgejo httpd
    ```

