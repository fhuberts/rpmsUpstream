# Migrating From Gitea To Forgejo

## Introduction

Migrating from Gitea to Forgejo is facilitated by this document and a few
scripts.

The migration was validated on the following versions:

| Package | Version                 |
| ------- | ----------------------- |
| Gitea   | 1.21.10+14-g4588c7b70   |
| Forgejo | 1.21.11.1+3-gf8be8e3b21 |

**WARNING**: Do not try to migrate from Gitea versions other than 1.21 to
Forgejo versions other than 1.21.

## Postgresql Preparation

Postgresql must be prepared for the migration by making sure the correct
collation is on the databases, otherwise the installation of Forgejo might
not succeed.

**Note**: Fixing the collation on the databases is an action that must be
performed during Postgresql upgrades as well.

Fix the collation on all databases by running:

```bash
sudo /usr/share/doc/forgejo/postgresql/postgresql-collation
```

## Stop Gitea

Stop Gitea:
```bash
systemctl stop gitea
```

## Install Forgejo

Install Forgejo:
```bash
sudo dnf install --allowerasing 'forgejo*'
```

## Migrate

Migrate from Gitea to Forgejo:
```bash
sudo /usr/share/doc/forgejo/migrating/migratefromgitea
```
