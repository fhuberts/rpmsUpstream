<?php

//
// Functions
//

// Report an error and exit with the specified code
function doError($code, $str) {
  error_log("ERROR: " . $str);
  echo "ERROR: $str\n";
  http_response_code($code);
  exit();
}


//
// Check Request
//

// Ensure that this is a POST request
if ($_SERVER["REQUEST_METHOD"] !== "POST") {
  doError(405, "Received a '" . $_SERVER["REQUEST_METHOD"] . "' request, expected a 'POST' request.");
}

// Ensure that the content type is JSON
$content_type = "";
if (isset($_SERVER["CONTENT_TYPE"])) {
  $content_type = strtolower(trim($_SERVER["CONTENT_TYPE"]));
}
if ($content_type !== "application/json") {
  doError(400, "Received non-JSON content (" . $content_type . ").");
}

// Ensure the payload is not empty
$payload = trim(file_get_contents("php://input"));
if (empty($payload)) {
  doError(400, "Did not receive payload.");
}

// Decode the JSON
$decoded = json_decode($payload, true);
if (json_last_error() !== JSON_ERROR_NONE) {
  doError(400, "Failed to decode the JSON payload, error: " . json_last_error());
}


//
// Main
//

// Get the Jenkins URL from the url parameter
$jenkinsUrl = "";
if (isset($_GET["url"])) {
  $jenkinsUrl = trim($_GET["url"]);
}
if (empty($jenkinsUrl)) {
  doError(400, "The 'url' parameter is not specified or empty");
}

# Check the Jenkins URL looks like an URL
if (filter_var($jenkinsUrl, FILTER_VALIDATE_URL) == false) {
  doError(400, "The 'url' parameter value '$jenkinsUrl' is not a valid URL.");
}

// Get the clone url type from the kind parameter
$jenkinsKind = "";
if (isset($_GET["kind"])) {
  $jenkinsKind = strtolower(trim($_GET["kind"]));
}
if (empty($jenkinsKind)) {
  doError(400, "The 'kind' parameter is not specified or empty");
}
if (($jenkinsKind == "http") || ($jenkinsKind == "https") || ($jenkinsKind == "html")) {
  $jenkinsKind = "html";
} else if ($jenkinsKind == "ssh") {
  $jenkinsKind = "ssh";
} else {
  doError(400, "Invalid 'kind' parameter '$jenkinsKind', use ssh, http, https or html.");
}

# Construct the Jenkins Git plugin URL
$jenkinsUrl .= "/git/notifyCommit?url=";
if ($jenkinsKind == "ssh") {
  $jenkinsUrl .= $decoded["repository"]["ssh_url"];
} else {
  $jenkinsUrl .= $decoded["repository"]["html_url"];
}

// Get the Jenkins access token from the token parameter
if (isset($_GET["token"])) {
  $jenkinsUrl .= "&token=" . trim($_GET["token"]);
}

# Perform a GET to the Jenkins Git plugin URL
$cURLConnection = curl_init();
curl_setopt($cURLConnection, CURLOPT_URL, $jenkinsUrl);
curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($cURLConnection, CURLOPT_TIMEOUT, 5);
curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, false);
$response = curl_exec($cURLConnection);

if ($response === false) {
  $info = curl_getinfo($cURLConnection);
  $returnCode = 500;
} else {
  $info = array("url"=>"$jenkinsUrl", "response"=>"$response");
  $returnCode = 200;
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($info, JSON_PRETTY_PRINT |  JSON_UNESCAPED_SLASHES);
http_response_code($returnCode);

curl_close($cURLConnection);

exit();
?>
