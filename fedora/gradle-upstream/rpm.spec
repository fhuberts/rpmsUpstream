%global __os_instTodoall_post       %{nil}
%global __requires_exclude_from ^%{_libexecdir}/gradle
%global __provides_exclude_from ^%{_libexecdir}/gradle

%global versionsub ""

Name:             gradle-upstream
Version:          8.13
Release:          1%{?dist}

License:          APL 2.0
URL:              http://gradle.org/
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch

BuildRequires:    unzip




#%package
Summary:          Gradle is a build system for the Java (JVM) world

Requires:         java >= 1.8.0
Requires:         java-devel >= 1.8.0

Obsoletes:        %{name}-samples < 6.1.1


%package docs
Summary:          Gradle is a build system for the Java (JVM) world
Requires:         %{name}


%package src
Summary:          Gradle is a build system for the Java (JVM) world
Requires:         %{name}


%description
Gradle is a build system that is a quantum leap for build technology in the
Java (JVM) world. Gradle provides:
- A very flexible general purpose build tool like Ant.
- Switchable, build-by-convention frameworks a la Maven, but without the
  lock-in!
- Very powerful support for multi-project builds.
- Very powerful dependency management (based on Apache Ivy).
- Full support for your existing Maven or Ivy repository infrastructure.
- Support for transitive dependency management without the need for remote
  repositories or pom.xml and ivy.xml files.
- Ant tasks and builds as first class citizens.
- Groovy build scripts.
- A rich domain model for describing your build. 


%description docs
Gradle build system documentation.


%description src
Gradle build system source code.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -pv "%{buildroot}%{_libexecdir}" "%{buildroot}%{_bindir}"
unzip -q -d "%{buildroot}%{_libexecdir}" gradle-docs.zip
unzip -q -d "%{buildroot}%{_libexecdir}" gradle-lib-plugins.zip
unzip -q -d "%{buildroot}%{_libexecdir}" gradle-lib.zip
unzip -q -d "%{buildroot}%{_libexecdir}" gradle-root.zip
unzip -q -d "%{buildroot}%{_libexecdir}" gradle-src.zip
mv -v "%{buildroot}%{_libexecdir}/gradle-%{version}%{versionsub}" "%{buildroot}%{_libexecdir}/gradle"
ln -sf "%{_libexecdir}/gradle/bin/gradle" "%{buildroot}%{_bindir}/gradle"


%files
%defattr(-,root,root)
%{_libexecdir}/gradle/{bin,init.d,lib,media,changelog.txt,getting-started.html,LICENSE,NOTICE,README}
%{_bindir}/gradle


%files docs
%defattr(-,root,root)
%{_libexecdir}/gradle/docs


%files src
%defattr(-,root,root)
%{_libexecdir}/gradle/src


%changelog
* Tue Feb 25 2025 Ferry Huberts - 8.13-1
- Gradle 8.13

* Fri Jan 24 2025 Ferry Huberts - 8.12.1-1
- Gradle 8.12.1

* Tue Dec 24 2024 Ferry Huberts - 8.12-1
- Gradle 8.12

* Thu Nov 21 2024 Ferry Huberts - 8.11.1-1
- Gradle 8.11.1

* Wed Sep 25 2024 Ferry Huberts - 8.10.2-1
- Gradle 8.10.2

* Thu Sep 19 2024 Ferry Huberts - 8.10.1-1
- Gradle 8.10.1

* Sat Aug 24 2024 Ferry Huberts - 8.10-1
- Gradle 8.10

* Thu Jul 18 2024 Ferry Huberts - 8.9-1
- Gradle 8.9

* Fri Jun 07 2024 Ferry Huberts - 8.8-1
- Gradle 8.8

* Wed Mar 27 2024 Ferry Huberts - 8.7-1
- Gradle 8.7

* Fri Feb 02 2024 Ferry Huberts - 8.6-1
- Gradle 8.6

* Tue Dec 05 2023 Ferry Huberts - 8.5-1
- Gradle 8.5

* Sun Oct 08 2023 Ferry Huberts - 8.4-1
- Gradle 8.4

* Sun Aug 20 2023 Ferry Huberts - 8.3-1
- Gradle 8.3

* Mon Jul 10 2023 Ferry Huberts - 8.2.1-1
- Gradle 8.2.1

* Sat Jul 01 2023 Ferry Huberts - 8.2-1
- Gradle 8.2

* Sun Apr 23 2023 Ferry Huberts - 8.1.1-1
- Gradle 8.1.1

* Thu Apr 13 2023 Ferry Huberts - 8.1-1
- Gradle 8.1

* Fri Mar 03 2023 Ferry Huberts - 8.0.2-1
- Gradle 8.0.2

* Tue Feb 21 2023 Ferry Huberts - 8.0.1
- Gradle 8.0.1

* Tue Feb 14 2023 Ferry Huberts - 8.0
- Gradle 8.0

* Fri Nov 25 2022 Ferry Huberts - 7.6
- Gradle 7.6

* Sat Aug 06 2022 Ferry Huberts - 7.5.1
- Gradle 7.5.1

* Fri Jul 15 2022 Ferry Huberts - 7.5
- Gradle 7.5

* Sat Apr 02 2022 Ferry Huberts - 7.4.2
- Gradle 7.4.2

* Sat Mar 19 2022 Ferry Huberts - 7.4.1
- Gradle 7.4.1

* Wed Feb 09 2022 Ferry Huberts - 7.4
- Gradle 7.4

* Fri Dec 24 2021 Ferry Huberts - 7.3.3
- Gradle 7.3.3

* Tue Dec 21 2021 Ferry Huberts - 7.3.2
- Gradle 7.3.2

* Thu Dec 02 2021 Ferry Huberts - 7.3.1
- Gradle 7.3.1

* Wed Nov 10 2021 Ferry Huberts - 7.3
- Gradle 7.3

* Tue Aug 17 2021 Ferry Huberts - 7.2
- Gradle 7.2

* Sat Jul 03 2021 Ferry Huberts - 7.1.1
- Gradle 7.1.1

* Tue Jun 15 2021 Ferry Huberts - 7.1
- Gradle 7.1

* Tue May 18 2021 Ferry Huberts - 7.0.2
- Gradle 7.0.2

* Thu Apr 15 2021 Ferry Huberts - 7.0
- Gradle 7.0

* Tue Feb 23 2021 Ferry Huberts - 6.8.3
- Gradle 6.8.3

* Fri Feb 05 2021 Ferry Huberts - 6.8.2
- Gradle 6.8.2

* Tue Jan 26 2021 Ferry Huberts - 6.8.1
- Gradle 6.8.1

* Sat Jan 09 2021 Ferry Huberts - 6.8
- Gradle 6.8

* Mon Nov 16 2020 Ferry Huberts - 6.7.1
- Gradle 6.7.1

* Wed Oct 14 2020 Ferry Huberts - 6.7
- Gradle 6.7

* Tue Aug 25 2020 Ferry Huberts - 6.6.1
- Gradle 6.6.1

* Thu Aug 13 2020 Ferry Huberts - 6.6
- Gradle 6.6

* Sun Jul 05 2020 Ferry Huberts - 6.5.1-2
- Bump to Fedora 32

* Wed Jul 01 2020 Ferry Huberts - 6.5.1
- Gradle 6.5.1

* Wed Jun 03 2020 Ferry Huberts - 6.5
- Gradle 6.5

* Sat May 16 2020 Ferry Huberts - 6.4.1
- Gradle 6.4.1

* Wed May 06 2020 Ferry Huberts - 6.4
- Gradle 6.4

* Thu Mar 26 2020 Ferry Huberts - 6.3
- Gradle 6.3

* Mon Mar 09 2020 Ferry Huberts - 6.2.2
- Gradle 6.2.2

* Wed Feb 26 2020 Ferry Huberts - 6.2.1
- Gradle 6.2.1

* Mon Feb 17 2020 Ferry Huberts - 6.2
- Gradle 6.2

* Sat Jan 25 2020 Ferry Huberts - 6.1.1
- Gradle 6.1.1

* Sat Nov 30 2019 Ferry Huberts - 6.0.1-2
- Fix rpm spec requires

* Tue Nov 19 2019 Ferry Huberts - 6.0.1
- Gradle 6.0.1

* Fri Nov 08 2019 Ferry Huberts - 6.0
- Gradle 6.0

* Sat Nov 02 2019 Ferry Huberts - 5.6.4
- Gradle 5.6.4

* Fri Oct 18 2019 Ferry Huberts - 5.6.3
- Gradle 5.6.3

* Thu Sep 12 2019 Ferry Huberts - 5.6.2
- Gradle 5.6.2

* Wed Aug 28 2019 Ferry Huberts - 5.6.1
- Gradle 5.6.1

* Fri Aug 16 2019 Ferry Huberts - 5.6
- Gradle 5.6

* Mon Jul 15 2019 Ferry Huberts - 5.5.1
- Gradle 5.5.1

* Mon Jul 01 2019 Ferry Huberts - 5.5
- Gradle 5.5

* Fri Apr 19 2019 Ferry Huberts - 5.4
- Gradle 5.4

* Sat Mar 30 2019 Ferry Huberts - 5.3.1
- Gradle 5.3.1

* Thu Mar 21 2019 Ferry Huberts - 5.3
- Gradle 5.3

* Sun Feb 10 2019 Ferry Huberts - 5.2.1
- Gradle 5.2.1

* Mon Feb 04 2019 Ferry Huberts - 5.2
- Gradle 5.2

* Fri Jan 11 2019 Ferry Huberts - 5.1.1
- Gradle 5.1.1

* Fri Jan 04 2019 Ferry Huberts - 5.1
- Gradle 5.1

* Tue Nov 27 2018 Ferry Huberts - 5.0
- Gradle 5.0

* Wed Oct 10 2018 Ferry Huberts - 4.10.2
- Gradle 4.10.2

* Mon Jul 16 2018 Ferry Huberts - 4.9
- Gradle 4.9

* Tue Jun 26 2018 Ferry Huberts - 4.8.1
- Gradle 4.8.1

* Mon Jun 04 2018 Ferry Huberts - 4.8
- Gradle 4.8

* Mon Apr 30 2018 Ferry Huberts - 4.7
- Gradle 4.7

* Wed Feb 28 2018 Ferry Huberts - 4.6
- Gradle 4.6

* Sun Feb 11 2018 Ferry Huberts - 4.5.1
- Gradle 4.5.1

* Mon Jan 29 2018 Ferry Huberts - 4.5
- Gradle 4.5

* Mon Dec 25 2017 Ferry Huberts - 4.4.1
- Gradle 4.4.1

* Wed Dec 06 2017 Ferry Huberts - 4.4
- Gradle 4.4

* Wed Nov 08 2017 Ferry Huberts - 4.3.1
- Gradle 4.3.1

* Sat Nov 04 2017 Ferry Huberts - 4.3
- Gradle 4.3
- Require openjdk-devel

* Wed Oct 25 2017 Ferry Huberts - 4.2.1
- Gradle 4.2.1

* Wed Aug 09 2017 Ferry Huberts - 4.1
- Gradle 4.1

* Thu Jul 27 2017 Ferry Huberts - 4.0.2
- Gradle 4.0.2

* Mon Jul 10 2017 Ferry Huberts - 4.0.1
- Gradle 4.0.1

* Fri Jun 16 2017 Ferry Huberts - 4.0
- Gradle 4.0

* Wed Apr 12 2017 Ferry Huberts - 3.5
- Gradle 3.5

* Mon Mar 06 2017 Ferry Huberts - 3.4.1-2
- Gradle 3.4.1

* Mon Feb 20 2017 Ferry Huberts - 3.4
- Gradle 3.4

* Mon Jan 09 2017 Ferry Huberts - 3.3
- Gradle 3.3

* Tue Dec 06 2016 Ferry Huberts - 3.2.1-2
- Bump to Fedora 25

* Wed Nov 30 2016 Ferry Huberts - 3.2.1
- Gradle 3.2.1

* Mon Nov 14 2016 Ferry Huberts - 3.2
- Gradle 3.2

* Tue Oct 04 2016 Ferry Huberts - 3.1
- Gradle 3.1

* Mon Aug 15 2016 Ferry Huberts - 3.0
- Gradle 3.0

* Wed Aug 03 2016 Ferry Huberts - 2.14
- Gradle 2.14.1

* Mon Jul 18 2016 Ferry Huberts - 2.13-2
- Fix pulling in fake dependencies because of new OSGi dependency analysis on
  Fedora 24.

* Mon Apr 25 2016 Ferry Huberts - 2.13
- Gradle 2.13

* Mon Mar 14 2016 Ferry Huberts - 2.12
- Initial release
