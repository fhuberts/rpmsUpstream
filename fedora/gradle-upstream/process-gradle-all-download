#!/usr/bin/bash

set -e
set -u

VERBOSE='-v'

ZIPFILES=(
  "gradle-docs.zip"
  "gradle-lib-plugins.zip"
  "gradle-lib.zip"
  "gradle-root.zip"
  "gradle-src.zip"
)

if [[ $# -ne 1 ]]; then
  echo "ERROR: specify the downloaded zip file."
  exit 1
fi

ZIPFILEIN="$1"

if [[ ! -r "$ZIPFILEIN" ]]; then
  echo "ERROR: zip file '$ZIPFILEIN' not readable."
  exit 1
fi

ZIPFILEINBASE="$(basename "$ZIPFILEIN" 2> /dev/null)"
ZIPFILEINBASE="${ZIPFILEINBASE,,}"

REGEX='^gradle-([[:digit:]]+(\.[[:digit:]]+)+)-all\.zip$'

if [[ -z "$( \
  echo "$ZIPFILEINBASE" | \
  grep -E "$REGEX" 2> /dev/null)" ]]; then
  echo "ERROR: zip filename doesn't comply to regular expression '$REGEX'"
  exit 1
fi

VERSION="$( \
  echo "$ZIPFILEINBASE" | \
  sed -r "s/$REGEX/\1/" 2> /dev/null)"

USERNAMEFULL="$( \
  getent passwd "$(whoami 2> /dev/null)" | \
  awk -F ':' '{print $5}' 2> /dev/null)"




rpmdev-bumpspec \
  -u "$USERNAMEFULL" \
  -c "Gradle $VERSION" \
  -n "$VERSION" \
  "rpm.spec"

rm -f $VERBOSE "${ZIPFILES[@]}"

for ZIPFILE in "${ZIPFILES[@]}"; do
  cp $VERBOSE "$ZIPFILEIN" "$ZIPFILE"
done

zip \
  -x '/gradle-*/docs/*' \
  -d \
  gradle-docs.zip \
  '*'

zip \
  -x '/gradle-*/lib/agents/*' \
  -x '/gradle-*/lib/plugins/*' \
  -d \
  gradle-lib-plugins.zip \
  '*'

zip \
  -x '/gradle-*/lib/*' \
  -d \
  gradle-lib.zip \
  '*'
zip \
  -d \
  gradle-lib.zip \
  '/gradle-*/lib/agents/*' \
  '/gradle-*/lib/plugins/*'

zip \
  -x '/gradle-*/src/*' \
  -d \
  gradle-src.zip \
  '*'

zip \
  -d \
  gradle-root.zip \
  '/gradle-*/docs/*' \
  '/gradle-*/lib/*' \
  '/gradle-*/src/*'


cat << "EOF"

Checking the original ZIP file against the split-out ZIP files...
EOF

TMPFILESORG="$(mktemp 2> /dev/null)"
zip -sf "$ZIPFILEIN" 2> /dev/null | \
  head -n -1 2> /dev/null | \
  tail -n +2 2> /dev/null | \
  sort 2> /dev/null \
  > "$TMPFILESORG"

TMPFILESNEW="$(mktemp 2> /dev/null)"
for ZIPFILE in "${ZIPFILES[@]}"; do
  zip -sf "$ZIPFILE" 2> /dev/null | \
    head -n -1 2> /dev/null | \
    tail -n +2 2> /dev/null | \
    sort 2> /dev/null \
    >> "$TMPFILESNEW"
done

TMPFILESNEWSORTED="$(mktemp 2> /dev/null)"
sort "$TMPFILESNEW" > "$TMPFILESNEWSORTED"
rm -f "$TMPFILESNEW"

diff -urN "$TMPFILESORG" "$TMPFILESNEWSORTED" &> /dev/null
declare -i result=$?
if [[ $result -ne 0 ]]; then
  cat << "EOF"
ERROR: Differences detected between original and split-out ZIP files.

DIFF:
EOF
  diff -urN "$TMPFILESORG" "$TMPFILESNEWSORTED" 2> /dev/null
  rm -f "$TMPFILESORG" "$TMPFILESNEWSORTED"
  exit 1
fi

rm -f "$TMPFILESORG" "$TMPFILESNEWSORTED"
echo "OK"


git add "rpm.spec" "${ZIPFILES[@]}"
git commit -s -m "gradle: upgrade to $VERSION"
