%define debug_package   %{nil}
%global _hardened_build 1

%global embeddedTar     %{name}.tar.bz2

Name:           discover
Version:        1.0.0
Release:        1.0.gf2473f8%{?dist}

License:        Unknown
URL:            https://gitlab.com/fhuberts/rpmsUpstream
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  libpcap-devel
BuildRequires:  make
BuildRequires:  qt-devel
BuildRequires:  qtchooser




#%package
Summary:        A real-time monitoring and analysis tool for IEC 61850-9-2 LE Sampled Values

Requires:       bzip2-libs
Requires:       fontconfig
Requires:       freetype
Requires:       glib2
Requires:       graphite2
Requires:       harfbuzz
Requires:       libbrotli
Requires:       libffi
Requires:       libibverbs
Requires:       libICE
Requires:       libnl3
Requires:       libpcap
Requires:       libpng
Requires:       libSM
Requires:       libstdc++
Requires:       libuuid
Requires:       libX11
Requires:       libXau
Requires:       libxcb
Requires:       libXcursor
Requires:       libXext
Requires:       libXfixes
Requires:       libXi
Requires:       libXinerama
Requires:       libxml2
Requires:       libXrandr
Requires:       libXrender
Requires:       pcre
Requires:       qt
Requires:       qt-x11
Requires:       xz-libs
Requires:       zlib


%description
A real-time monitoring and analysis tool for IEC 61850-9-2 LE Sampled Values.

Upstream     : http://stevenblair.github.io/discover/
Upstream code: https://github.com/stevenblair/discover


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}
mkdir build
cd build
tar jxvf "../%{embeddedTar}"


%build
export PATH=$PATH:/usr/lib/qtchooser
cd build
qmake -qt=4 -makefile CONFIG+=release discover.pro
make


%install
cd build
rm -rfv "%{buildroot}"
mkdir -pv \
  "%{buildroot}" \
  "%{buildroot}%{_bindir}" \
  "%{buildroot}%{_datarootdir}/%{name}/icons"

cp -pdrv -t "%{buildroot}"                              "../usr"
cp -pdrv -t "%{buildroot}%{_bindir}"                    "%{name}"
cp -pdrv -t "%{buildroot}%{_datarootdir}/%{name}"       "README.md"
cp -pdrv -t "%{buildroot}%{_datarootdir}/%{name}/icons" "icon.ico"


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

#%post


%clean
rm -rf "%{buildroot}"


%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_datarootdir}/applications/*
%{_datarootdir}/%{name}/README.md
%{_datarootdir}/%{name}/icons/*


%changelog
* Fri Aug 20 2021 Ferry Huberts - 1.0.0-1.0.gf2473f8
- Update to 1.0.0-0-gf2473f8
